export function isValidIP(adr) {
    const nums = adr.split('.')

    if (nums.length !== 4) return false

    const numLeadingZeros = nums.filter(n => n.charAt(0) === '0')
    
    if (numLeadingZeros.length !== 0) return false

    const numRangeViolations = nums.map(n => parseInt(n, 10)).filter(n => (n > 255 || n < 0))
    if (numRangeViolations.length !== 0) return false

    const numLastDigitZero = nums.filter(n => n.charAt(n.length-1) === '0')
    if (numLastDigitZero.length !== 0) return false

    return true;
}