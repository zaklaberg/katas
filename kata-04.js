export function isBoomerang(arr) {
    if (arr.length != 3) return false

    if (arr[0] === arr[2] && arr[1] != arr[0]) return true
    return false
}

export function countBoomerangs(arr) {
    if (arr.length < 3) return 0
    return arr.reduce((acc, _, idx, arr) => isBoomerang(arr.slice(idx, idx+3)) ? ++acc : acc, 0)
}