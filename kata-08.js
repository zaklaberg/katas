export function reorderReg(word) {
    let uppercase = word.match(/[A-Z]/g)
    if (uppercase) uppercase = uppercase.join('')
    else uppercase = ''

    let lowercase = word.match(/[a-z]/g)
    if (lowercase) lowercase = lowercase.join('')
    else lowercase = ''

    let numbers = word.match(/[0-9]/g)
    if (numbers) numbers = numbers.join('')
    else numbers = ''

    let remainder = word.match(/[^a-z0-9]/gi)
    if (remainder) remainder = remainder.join('')
    else remainder = ''

    return uppercase + lowercase + remainder + numbers
}