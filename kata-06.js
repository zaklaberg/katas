function getAtIndexOrLast(arr, idx) {
    if (idx < arr.length) return arr[idx]
    return arr[arr.length-1]
}

function wordToHints(str) {
    const hint = str.replace(/[^ ]/g, '_')
    const hints = [...str].map((e,i) => str.substr(0, i) + hint.substr(i, hint.length))
    hints.push(str)
    return hints
}

export function sentenceToHints(str) {
    const wordHints = str.split(' ').map(word => wordToHints(word))
    
    let numHints = 0
    wordHints.forEach(e => {
    	if (e.length > numHints) numHints = e.length
    })
    
    let hints = []
    for(let i = 0; i < numHints; ++i) {
    	hints.push(wordHints.map(e => getAtIndexOrLast(e, i)).join(' '))
    }
    return hints
}