export function palseq(num) {
    for(let seed = 1; seed < num; ++seed) {
        let mirrors = 0
        let currSeed = seed
        let steps = 1
        const maxSteps = 50
        while(steps < maxSteps && mirrors <= num) {
            mirrors = currSeed + reverseNumber(currSeed)
            if (isPalindrome(mirrors) && mirrors !== num) break
            if (mirrors === num && !isPalindrome(seed)) {
                return [seed, steps]
            }
            currSeed = mirrors
            ++steps
        }
    }
    return [num, 0]
}

export function isPalindrome(num) {
    return num === reverseNumber(num)
}

export function reverseNumber(num) {
    return Number([...String(num)].reverse().join(''))
}