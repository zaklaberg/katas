export function squareDigits(num) {
    return [...String(num)].map(n => n**2).reduce((acc, cv) => acc+cv)
}

export function isHappy(num) {
    const prevSeq = [num]

    let sq = squareDigits(num)
    while(sq !== 1 && !prevSeq.includes(sq)) {
        prevSeq.push(sq)
        sq = squareDigits(sq)
    }
    
    if (sq === 1) return `HAPPY ${prevSeq.length}`
    
    // Find first time a number is repeated
    // While loop breaks on .includes() => last sq is not pushed 
    prevSeq.push(sq)

    // Maybe unclear, but yknow
    let firstRepeatedIndex = prevSeq.reduce((acc, n, i) => prevSeq.filter(e => e===n).length > 1 ? i : acc, 0)
    return `SAD ${firstRepeatedIndex}`
}