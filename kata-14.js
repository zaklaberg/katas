export function XAndO(board) {
    const rows = board.map(row => row.split('|'))
    const columns = [[], [], []]
    rows
        .forEach(row => row
            .forEach((e, colId) => columns[colId].push(e)))
    
    // There are 8 possible "winning directions"
    let winningPos = null
    winningPos = containsWinningArray(rows)
    if (winningPos) return winningPos.map(e => e+1)

    winningPos = containsWinningArray(columns)
    if (winningPos) return winningPos.map(e => e+1)

    const numX = arr => arr.filter(e => e === 'X').length
    const numO = arr => arr.filter(e => e === 'O').length
    
    const diags = [
        [rows[0][0], rows[1][1], rows[2][2]], 
        [rows[0][2], rows[1][1], rows[2][0]]
    ]
    if (numX(diags[0]) === 2 && !numO(diags[0])) {
        const nonXIdx = diags[0].findIndex(e => e === ' ')
        winningPos = [nonXIdx, nonXIdx]
    }
    if (winningPos) return winningPos.map(e => e+1)

    if (numX(diags[1]) === 2 && !numO(diags[1])) {
        const nonXIdx = diags[1].findIndex(e => e === ' ')
        if (nonXIdx === 0) winningPos = [0, 2]
        else if (nonXIdx === 1) winningPos = [1, 1]
        else winningPos = [2, 0]
    }
    if (winningPos) return winningPos.map(e => e+1)
}

function containsWinningArray(matrix) {
    const numX = arr => arr.filter(e => e === 'X').length
    const numO = arr => arr.filter(e => e === 'O').length

    // Let's check each row, then each column, then each diagonal
    let winningPos = null
    matrix.forEach((row, rowIdx) => {
        if (numX(row) === 2 && numO(row) === 0) {
            winningPos = [rowIdx, row.findIndex(e => e === ' ')]
        }
    })

    return winningPos
}