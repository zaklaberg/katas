import { maxProfit } from "../kata-15"

it('gets 14', () => {
    const ain = [8, 5, 12, 9, 19, 1]
    expect(maxProfit(ain)).toBe(14)
})

it('gets 9', () => {
    const ain = [2, 4, 9, 3, 8]
    expect(maxProfit(ain)).toBe(7)
})

it('gets 0', () => {
    const ain = [21, 12, 11, 9, 6, 3]
    expect(maxProfit(ain)).toBe(0)
})