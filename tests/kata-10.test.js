import { reverseNumber, palseq } from '../kata-10.js'

it('Reverse 152', () => {
    expect(reverseNumber(152)).toBe(251)
})

it('palseq 3113', () => {
    expect(palseq(3113)).toEqual([199, 3])
})

it('palseq 4884', () => {
    expect(palseq(4884)).toEqual([69, 4])
})

it('palseq 11', () => {
    expect(palseq(11)).toEqual([10, 1])
})

it('palseq 1', () => {
    expect(palseq(1)).toEqual([1, 0])
})

it('palseq large', () => {
    expect(palseq(8836886388)).toEqual([177, 15])
})