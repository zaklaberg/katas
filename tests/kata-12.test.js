import { squareDigits, isHappy } from '../kata-12'

it('squares correctly', () => {
    expect(squareDigits(139)).toBe(91)
})

it ('findshappy 139', () => {
    expect(isHappy(139)).toEqual('HAPPY 5')
})

it('is happy 1', () => {
    expect(isHappy(1)).toEqual('HAPPY 1')
})

it('findshappy 7', () => {
    expect(isHappy(7)).toEqual('HAPPY 5')
})

it('findsnothappy 67', () => {
    expect(isHappy(67)).toEqual('SAD 10')
})

it('not happy 89', () => {
    expect(isHappy(89)).toEqual('SAD 8')
})