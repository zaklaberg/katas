import { codeFromDateAndGender, codeFromSurname, codeFromName, fiscalCode } from '../kata-05.js'

describe('Fiscal code', () => {
    const matt = {
        name: 'Matt', 
        surname: 'Edabit', 
        gender: 'M',
        dob: '1/1/1900',
    }
    it('Codes Matt', () => {
        expect(fiscalCode(matt)).toBe('DBTMTT00A01')
    })

    const helen = {
        name: 'Helen', 
        surname: 'Yu', 
        gender: 'F', 
        dob: '1/12/1950'
    }
    it('Codes Helen', () => {
        expect(fiscalCode(helen)).toBe('YUXHLN50T41')
    })

    const mickey = {
        name: 'Mickey', 
        surname: 'Mouse',
        gender: 'M',
        dob: '16/1/1928'
    }
    it('Codes Mickey', () => {
        expect(fiscalCode(mickey)).toBe('MSOMKY28A16')
    })
})

describe('Codes surname correctly', () => {
    it('Counts 3 consonants', () => {
        expect(codeFromSurname('Newman')).toBe('NWM')
    })

    it('Counts <3 consonants', () => {
        expect(codeFromSurname('Fox')).toBe('FXO')
    })

    it('Counts <3 consonants v2', () => {
        expect(codeFromSurname('Hope')).toBe('HPO')
    })

    it('Counts <3 letters', () => {
        expect(codeFromSurname('Yu')).toBe('YUX')
    })
})

describe('Codes name correctly', () => {
    it('Counts 3 consonants', () => {
        expect(codeFromName('Matt')).toBe('MTT')
    })

    it('Counts >3 consonants', () => {
        expect(codeFromName('Samantha')).toBe('SNT')
    })

    it('Counts >3 consonants v2', () => {
        expect(codeFromName('Thomas')).toBe('TMS')
    })

    it('Counts <3 consonants', () => {
        expect(codeFromName('Bob')).toBe('BBO')
    })

    it('Counts <3 consonants v2', () => {
        expect(codeFromName('Paula')).toBe('PLA')
    })

    it('Counts <3 letters', () => {
        expect(codeFromName('Al')).toBe('LAX')
    })
})

describe('Codes dates and genders correctly', () => {
    it('stuff', () => {
        expect(codeFromDateAndGender('1/1/1900', 'M')).toBe('00A01')
    })
})