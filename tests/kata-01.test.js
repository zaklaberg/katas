import allElementsEqual from '../kata-01.js'

describe('Testing allElementsEqual function', () => {
    it('All equal @ symbols', () => {
        expect(allElementsEqual(['@', '@', '@', '@'])).toBe(true)
    });
    
    it('All equal lowercase words', () => {
        expect(allElementsEqual(['abc', 'abc', 'abc', 'abc'])).toBe(true)
    });
    
    it('All equal uppercase words', () => {
        expect(allElementsEqual(['SS', 'SS', 'SS', 'SS'])).toBe(true);
    });
    
    it('Symbol words of unequal length', () => {
        expect(allElementsEqual(['&&', '&', '&&&', '&&&&'])).toBe(false);
    });
    
    it('Words with equal letters but different case', () => {
        expect(allElementsEqual(['SS', 'SS', 'SS', 'Ss'])).toBe(false);
    });
});
