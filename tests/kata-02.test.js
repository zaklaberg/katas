import { shuffle, shuffleCount } from '../kata-02.js'

describe('Shuffle works', () => {
    it('Single shuffle', () => {
        expect(shuffle([1,2,3,4,5,6,7,8])).toEqual([1,5,2,6,3,7,4,8])
    });

    it('Double shuffle', () => {
        expect(shuffle(shuffle([1,2,3,4,5,6,7,8]))).toEqual([1,3,5,7,2,4,6,8])
    })

    it ('Triple shuffle', () => {
        expect(shuffle(shuffle(shuffle([1,2,3,4,5,6,7,8])))).toEqual([1,2,3,4,5,6,7,8]);
    })
});

describe('ShuffleCount', () => {
    it('Count', () => {
        expect(shuffleCount(8)).toBe(3);
    })

    it ('cc', () => {
        expect(shuffleCount(14)).toBe(12);
    })

    it ('cc2', () => {
        expect(shuffleCount(52)).toBe(8);
    })
});