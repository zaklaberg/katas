import { sentenceToHints } from '../kata-06.js'

describe('Grant hints', () => {
    it('Mary Queen of Scots', () => {
        const hints = [
            '____ _____ __ _____',
            'M___ Q____ o_ S____',
            'Ma__ Qu___ of Sc___',
            'Mar_ Que__ of Sco__',
            'Mary Quee_ of Scot_',
            'Mary Queen of Scots'
        ]
        expect(sentenceToHints('Mary Queen of Scots')).toEqual(hints)
    })

    it('The Life of Pi', () => {
        const hints = [
            '___ ____ __ __',
            'T__ L___ o_ P_', 
            'Th_ Li__ of Pi', 
            'The Lif_ of Pi', 
            'The Life of Pi'
        ]
        expect(sentenceToHints('The Life of Pi')).toEqual(hints)
    })
})