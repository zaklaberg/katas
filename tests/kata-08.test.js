 import { reorderReg, reorderAsc } from '../kata-08.js'

 describe('Regexp reorder: ', () => {
    it('hA2p4Py', () => {
        expect(reorderReg('hA2p4Py')).toBe('APhpy24')
    })
   
    it("m11oveMENT", () => {
        expect(reorderReg("m11oveMENT")).toBe("MENTmove11")
    })
   
    it("s9hOrt4CAKE", () => {
        expect(reorderReg("s9hOrt4CAKE")).toBe("OCAKEshrt94")
    })

    it('s7$!_ymb0lZ   TeS t', () => {
        expect(reorderReg('s7$!_ymb0lZ   TeS t')).toBe('ZTSsymblet$!_    70')
    })
 })
