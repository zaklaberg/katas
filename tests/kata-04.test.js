import { countBoomerangs } from '../kata-04.js'

describe('Boomerang counter', () => {
    it('Counts overlapping', () => {
        expect(countBoomerangs([9, 5, 9, 5, 1, 1, 1])).toBe(2)
    })

    it('Counts up', () => {
        expect(countBoomerangs([5, 6, 6, 7, 6, 3, 9])).toBe(1)
    })

    it('Doesnt count equal', () => {
        expect(countBoomerangs([4, 4, 4, 9, 9, 9, 9])).toBe(0)
    })

    it('Counts overlapping', () => {
        expect(countBoomerangs([1, 7, 1, 7, 1, 7, 1])).toBe(5)
    })

    it('Counts negative', () => {
        expect(countBoomerangs([3, 7, 3, 2, 1, 5, 1, 2, 2, -2, 2])).toBe(3)
    })
})