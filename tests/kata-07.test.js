import { isValidIP } from '../kata-07.js'

describe('Check valid IPv4', () => {
    it('1.2.3.4', () => {
        expect(isValidIP('1.2.3.4')).toBe(true)
    })

    it('1.2.3', () => {
        expect(isValidIP('1.2.3')).toBe(false)
    })

    it('1.2.3.4.5', () => {
        expect(isValidIP('1.2.3.4.5')).toBe(false)
    })

    it('123.45.67.89', () => {
        expect(isValidIP('123.45.67.89')).toBe(true)
    })

    it('123.456.78.90', () => {
        expect(isValidIP('123.456.78.90')).toBe(false)
    })

    it('123.045.067.089', () => {
        expect(isValidIP('123.045.067.089')).toBe(false)
    })

})