import { XAndO } from '../kata-14'

it('finds[1,3]', () => {
    let board = [" | | ", " |X| ", "X| | "]
    expect(XAndO(board)).toEqual([1,3])
})

it('finds [3,3]', () => {
    let board = ["X|X|O", "O|X| ", "X|O| "]
    expect(XAndO(board)).toEqual([3,3])
})