import { getLength } from '../kata-11.js'

it('[1, [2,3]', () => {
    expect(getLength([1, [2,3]])).toBe(3)
})

it('[1, [2, [3,4]]]', () => {
    expect(getLength([1, [2, [3, 4]]])).toBe(4)
})

it('[1, [2, [3, [4, [5, 6]]]]]', () => {
    expect(getLength([1, [2, [3, [4, [5, 6]]]]])).toBe(6)
})

it('[1, [2], 1, [2], 1]', () => {
    expect(getLength([1, [2], 1, [2], 1])).toBe(5)
})

it('[]', () => {
    expect(getLength([])).toBe(0)
})