import { sLargest } from '../kata-13'

it('finds 40', () => {
    expect(sLargest([10, 40, 30, 20, 50])).toBe(40)
})

it('finds 105', () => {
    expect(sLargest([25, 143, 89, 13, 105])).toBe(105)
})

it('finds 23', () => {
    expect(sLargest([54, 23, 11, 17, 10])).toBe(23)
})
