import { noStrangers } from '../kata-03.js'

describe('Count aquaintances', () => {
    it('Aquaintances with punctuation', () => {
        const str = "Se~~e Sp??!,.ot run. Se..,e S`pot.. jum!p. Sp)($ot likes jumping. See Sp^^.--ot fly.";
        expect(noStrangers(str)).toEqual([["spot", "see"], []]);
    });

    /*it('Aqs and friends', () => {
        const str = "I'm a friend friend friend friend friend but you're not not not";
        expect(noStrangers(str)).toEqual([['not'], ['friend']])
    })*/

    
    
});