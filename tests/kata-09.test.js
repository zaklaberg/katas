import { howUnlucky } from '../kata-09.js'

it('2020', () => {
    expect(howUnlucky(2020)).toBe(2)
})

it('2026', () => {
    expect(howUnlucky(2026)).toBe(3)
})

it('2016', () => {
    expect(howUnlucky(2016)).toBe(1)
})