const allElementsEqual = arr => arr.every(e => e === arr[0]);

export default allElementsEqual;