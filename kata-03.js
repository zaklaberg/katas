export function stripPunctuation(s) {
    return s.replace(/[.,\/#!$?%\^&\*;:{}=\-_`~()]/g,"");
}

function byAquaintanceOccurrence(a, b) {
    if (a[1].aquaintance === b[1].aquaintance) return 0;
    return a[1].aquaintance < b[1].aquaintance ? -1 : 1;
}
function byFriendOccurrence(a, b) {
    if (a[1].friend === b[1].friend) return 0;
    return a[1].friend < b[1].friend ? -1 : 1;
}

export function noStrangers(strIn) {
    const words = stripPunctuation(strIn.toLowerCase()).split(" ");
    let wordCountAndOccurence = {};

    let acCount = 0;
    let frCount = 0;
    words.forEach((word, idx) => {
        if (!wordCountAndOccurence[word]) {
            wordCountAndOccurence[word] = {count: 0, aquaintance: 1000, friend: 1000 }
        }
        wordCountAndOccurence[word].count++;

        if (wordCountAndOccurence[word].count == 3) {
            wordCountAndOccurence[word].aquaintance = acCount++;
        }
        else if(wordCountAndOccurence[word].count === 5) {
            wordCountAndOccurence[word].friend = frCount++;
        }
    })

    //return wordCountAndOccurence;

    wordCountAndOccurence = Object.entries(wordCountAndOccurence);

    // Lodash remove would save us double iteration, but w/e
    const friends = wordCountAndOccurence
                    .filter(e => e[1].count >= 5)
                    .sort(byFriendOccurrence)
                    .map(e => e[0]);
    const aquaintances = wordCountAndOccurence
                            .filter(e => e[1].count >= 3 && e[1].count < 5)
                            .sort(byAquaintanceOccurrence)
                            .map(e => e[0])
    return [aquaintances, friends];
}