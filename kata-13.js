export function sLargest(arr) {
    let max = 0
    let nmax = -1

    for(let i = 0; i < arr.length; ++i) {
        if (arr[i] > max) {
            nmax = max
            max = arr[i]
        }
        else if (arr[i] > nmax) {
            nmax = arr[i]
        }
    }

    return nmax
}