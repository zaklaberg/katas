function deepFlatten(arr) {
    return  arr.reduce((acc, cv) => Array.isArray(cv) ? acc.concat(deepFlatten(cv)) : [...acc, cv], [])
}

export function getLength(arr) { 
    return deepFlatten(arr).length
}