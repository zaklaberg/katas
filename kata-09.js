export function howUnlucky(year) {
    return [...Array(12).keys()].filter(month => new Date(year, month, 13).getDay() === 5).length
}