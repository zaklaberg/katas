function shuffle(deck) {
    let shuffled = [];
    let midIdx = Math.floor(deck.length / 2);
    
    for(let i = 0; i < midIdx; ++i) {
        shuffled.push(deck[i]);
        shuffled.push(deck[midIdx + i]);
    }

    return shuffled;
}

function shuffle2(deck) {
    return deck.map((e,i,v) => {
        let midIdx = Math.floor(v.length / 2);
        return i >= midIdx ? undefined : [e, [v[midIdx + i]]];
    }).filter(e => e !== undefined).flat(2);
}

function shuffleCount(cardsInDeck) {
    let deck = Array(cardsInDeck).fill().map((e,i) => i+1);
    let shuffled = shuffle2(deck);
    let count = 1;
    let maxIter = 10000;

    while(JSON.stringify(deck) !== JSON.stringify(shuffled)) {
        shuffled = shuffle2(shuffled);
        ++count;

        if (count > maxIter) break;
    }
    return count;
}

export { shuffle, shuffleCount }