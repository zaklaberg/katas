function isVowel(char) {
    if (char.length === 1) {
        return /[aeiou]/.test(char) // may need to add y and w also
    }
}

export function codeFromSurname(surname) {
    surname = surname.toLowerCase()
    const vowels = [...surname].filter(letter => isVowel(letter))
    const consonants = [...surname].filter(letter => !isVowel(letter))

    if (consonants.length >= 3) return consonants.slice(0, 3).join('').toUpperCase()

    let code = consonants
    vowels.forEach(vowel => {
        if (code.length === 3) return
        code.push(vowel)
    })

    if (code.length === 3) return code.join('').toUpperCase()

    while(code.length < 3) code.push('x')
    return code.join('').toUpperCase()
}

export function codeFromName(name) {
    name = name.toLowerCase()
    const vowels = [...name].filter(letter => isVowel(letter))
    const consonants = [...name].filter(letter => !isVowel(letter))

    if (consonants.length > 3) return [consonants[0], consonants[2], consonants[3]].join('').toUpperCase()
    if (consonants.length === 3) return consonants.join('').toUpperCase()

    let code = consonants
    vowels.forEach(vowel => {
        if (code.length === 3) return
        code.push(vowel)
    })

    if (code.length === 3) return code.join('').toUpperCase()

    while(code.length < 3) code.push('x')
    return code.join('').toUpperCase()
}

export function codeFromDateAndGender(date, gender) {
    const monthMap = { 1: 'A', 2: 'B', 3: 'C', 4: 'D', 5: 'E', 6: 'H', 7: 'L', 8: 'M', 9: 'P', 10: 'R', 11: 'S', 12: 'T' }

    date = date.split('/')
    const day = parseInt(date[0], 10)
    const codeFromYear = date[2].substr(2,3)
    const codeFromMonth = monthMap[parseInt(date[1], 10)]
    let codeFromDay = ''

    if (gender === 'M') {
        if (day < 10) codeFromDay = `0${day}`
        else codeFromDay = `${day}`
    }
    else {
        codeFromDay = `${day+40}`
    }

    return codeFromYear+codeFromMonth+codeFromDay
}

export function fiscalCode(person) {
    return codeFromSurname(person.surname) + codeFromName(person.name) + codeFromDateAndGender(person.dob, person.gender)
}